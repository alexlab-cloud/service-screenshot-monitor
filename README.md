# screenshot-monitor

A basic solution for monitoring Alexlab Cloud's live services with [gowitness](https://github.com/sensepost/gowitness)
and [GitLab](https://gitlab.com). Set up using [this GitLab blog article](https://about.gitlab.com/blog/2023/01/11/monitor-web-attack-surface-with-gitlab/#submitted-message)
from [Chris Moberly](https://about.gitlab.com/blog/authors/cmoberly/).
