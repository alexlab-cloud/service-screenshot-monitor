#!/usr/bin/env bash

# Get targets from a CI variable file
TARGETS=$TARGET_FILE
echo $TARGETS
# Create output directory
mkdir ./targets

# Identify web services
echo "Identifying web services across $(cat "$TARGETS" | wc -l) targets..."
cat "$TARGETS" | bin/httpx -o targets/web-services.txt -p 80,443

echo "Discovered $(cat targets/web-services.txt | wc -l) web services."
