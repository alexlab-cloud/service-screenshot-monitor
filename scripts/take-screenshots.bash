#!/usr/bin/env bash

# Run gowitness
bin/gowitness file -f targets/web-services.txt
bin/gowitness report export -f report.zip

# Move the report to pages' outdir
unzip report.zip
mv gowitness public
