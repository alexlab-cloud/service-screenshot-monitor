#!/usr/bin/env bash

HTTPX_VER="1.3.7"
GOWITNESS_VER="2.5.1"

# Create folder for downloaded binaries
mkdir bin

# Install general prereqs
apt -qq update > /dev/null
apt -qq install -y wget unzip > /dev/null

# Install prereqs for gowitness
apt -qq install -y chromium > /dev/null

# Download tools
wget -q https://github.com/projectdiscovery/httpx/releases/download/v${HTTPX_VER}/httpx_${HTTPX_VER}_linux_amd64.zip
wget -q https://github.com/sensepost/gowitness/releases/download/${GOWITNESS_VER}/gowitness-${GOWITNESS_VER}-linux-amd64

# Unzip + move all relases to bin folder
unzip httpx_${HTTPX_VER}_linux_amd64.zip -d bin/
mv gowitness-${GOWITNESS_VER}-linux-amd64 bin/gowitness
chmod u+x bin/gowitness
